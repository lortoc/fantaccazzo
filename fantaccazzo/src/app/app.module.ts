
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DashboardModule} from './dashboard.module';
import { BetModule } from './bet.module';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BetComponent } from './components/bet/bet.component';

import { UserService } from './services/impl/user/user.service';
import {PlayerService} from './services/impl/player/player.service';
import { BetService } from './services/impl/bet/bet.service';
import { MessageService } from './services/impl/message/message.service';
import {TeamService} from './services/impl/team/team.service';
import {PuntataService} from './services/impl/puntata/puntata.service';
import { UserModule } from './user.module';

@NgModule({
  declarations: [
    AppComponent,
  ],

  imports: [
    BrowserModule, DashboardModule, AppRoutingModule, BetModule, FormsModule, UserModule
  ],
  providers: [UserService, MessageService,  PlayerService, BetService, TeamService, PuntataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
