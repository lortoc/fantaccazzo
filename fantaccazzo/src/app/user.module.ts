import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryUserService } from './services/mocks/in-memory-users.service';
import { AuthenticationService } from './services/impl/authentication/authentication.service';
import { UserService } from './services/impl/user/user.service';
import { RegisterComponent } from './components/register/register.component';
import { InMemoryDataService } from './services/mocks/in-memory-data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
   LoginComponent,
   RegisterComponent
  ],
  providers: [AuthenticationService, UserService]
})
export class UserModule { }
