import {Puntata} from './puntata';
import { User } from './user';


export class Bet {
   public id: number;
   public user: User;
   public puntate: Puntata[];
  
  public constructor(){
    this.user = new User();
    this.puntate = [];
  }
  
  public setUser(user:User){
    this.user = user;
  }
  
  public addPuntata(puntata: Puntata){
    this.puntate.push(puntata);
    console.log("puntate");

    console.log(this.puntate);
  }

  public getPuntate () {
    return this.puntate;
  }
}

