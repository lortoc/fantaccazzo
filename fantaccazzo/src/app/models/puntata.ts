import {Player} from './player';

export class Puntata {
    public id: number;
  public player: Player;
  public nGol: number;
  public status: String;

  public constructor(player?: Player, nGol?: number) {
    if (player) 
      this.setPlayer(player);
    if (nGol) 
      this.setNGol(nGol);
    this.status = 'pending';
  }

  public getPlayer(): Player {
    return this.player;
  }
  
  public getNGol(): number {
    return this.nGol;
  }
  
  public setPlayer(player: Player): void {
    this.player = player;
  }
  
  public setNGol(nGol: number): void {
    this.nGol = nGol;
  }
  
  public stringify(): String {
    return this.player.name + ' - ' + this.player.ruolo + ' goals: ' + this.nGol;
  }
}

