import { Player } from './player';
import { User } from './user';


export class Team {
  public id: number;
  public name: string;
  public user: User;
  public players: Player[];

}
