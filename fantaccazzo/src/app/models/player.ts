export class Player {
  public id: number;
  public name: string;
  public ruolo: string;
  public goals: number;
}
