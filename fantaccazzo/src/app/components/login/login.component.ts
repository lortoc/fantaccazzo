import { Message } from '../../models/message';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/impl/authentication/authentication.service';
import { MessageService } from '../../services/impl/message/message.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  message: Message;

  constructor(private authenticationService: AuthenticationService,
              private messageService: MessageService,
              private router: Router, private fb: FormBuilder) {
    
       this.myForm = fb.group({
            'username': ['', Validators.required],
            'password': ['', Validators.required]
       });
  }

  ngOnInit() {
    console.log('Dentro LoginComponent->ngOnInit()... ');
    this.authenticationService.logout();
    
  }
  
  clearData(){
    console.log('Dentro LoginComponent->clearData()... ');
    this.myForm.reset();
    
  }

  login(form: any) {
      console.log('LoginComponent->login(), username: ', form.username);
      console.log('LoginComponent->login(), password: ', form.password);
      
      this.authenticationService.login(form.username, form.password)
                  .subscribe(
                    users => {
                      
                      console.log('LoginComponent->login(), user = ', users);
                       
                      if(users[0] != undefined){
                        
                        let found: boolean = false;
                        for(let current of users){
                          
                          if(current.userName == form.username &&
                             current.passWord == form.password){
                            
                                found = true;
                                break;
                          }
                        }
                        console.log('found = ', found);
                        
                        if(found){
                          
                          let userData = users[0];
                          console.log('LoginComponent->login(), userData = ', userData);
                          localStorage.setItem('currentUser', JSON.stringify(userData));
                          this.router.navigate(['/dashboard']);
                        }
                        else{
                          this.message = this.messageService.error('username e/o password non validi');
                        }
                      }
                      else{
                        this.message = this.messageService.error('username e/o password non validi');
                      }
                      
                      
                    },
                    error => {
                      this.message = this.messageService.error('subscribe error');
                    });
      
  }
  
  register(){
    this.router.navigate(['/register']);
  }
  
    
}
