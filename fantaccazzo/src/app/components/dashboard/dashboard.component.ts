import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import {UserService } from '../../services/impl/user/user.service';

import { User } from '../../models/user';
import { Player } from '../../models/player';
import {Bet} from '../../models/bet';
import { AuthenticationService } from '../../services/impl/authentication/authentication.service';

import {PlayerService} from '../../services/impl/player/player.service';
import {BetService} from '../../services/impl/bet/bet.service' ;

import { Location } from '@angular/common';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

constructor(private betService: BetService, private playerService: PlayerService,
            private authenticationService: AuthenticationService,
            private userService: UserService, private router: Router) {
  
                this.getUser();
}

  protected user: User;
  protected bets: Bet [] = [];

  ngOnInit() {

  }

teams() { console.log('teams called'); }

  getBets(): void {
    this.betService.getBets().subscribe(bets => this.bets = bets);
  }


getUser() {
  this.user = this.userService.getLoggedUser();
}

  logout() {
    this.authenticationService.logout();
     this.router.navigate(['/login']);
  }

goToBets() {
 console.log('bets called');
 this.router.navigate(['/bets']);
 }


goToAdminArea() {
  this.router.navigate (['/admin_area']);
}


}


