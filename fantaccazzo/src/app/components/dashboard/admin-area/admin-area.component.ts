  import { Component, OnInit, ViewEncapsulation } from '@angular/core';
  import { Router } from '@angular/router';

  import {Player} from '../../../models/player';
  import {Bet} from '../../../models/bet';
  import {Puntata} from '../../../models/puntata';

  import {PlayerService} from '../../../services/impl/player/player.service';
  import {BetService} from '../../../services/impl/bet/bet.service';

  @Component({
    selector: 'app-admin-area',
    templateUrl: './admin-area.component.html',
    styleUrls: ['./admin-area.component.css'],
    encapsulation: ViewEncapsulation.None
  })

  export class AdminAreaComponent implements OnInit {

    constructor(private playerService: PlayerService,
                private betService: BetService,
                private router: Router,
           ) { }



    private players: Player [];
    private bets: Bet [];
    protected info;
    betPuntateMap: Map<Bet, Puntata []> = new Map<Bet, Puntata  []>();

    ngOnInit() {
          this.getPlayers();
          this.getBets();
   }

    getPlayers () {
      this.playerService.getPlayers().subscribe (players => this.players = players);
    }

    reload () {
         this.router.navigate (['/admin_area']);
    }

   goToDashboard() {
         this.router.navigate (['/dashboard']);
    }

    validateBets() {
       let i: number;
        for (i = 0; i < this.players.length; i++) {
          this.validateBet (this.players[i]);
        }
      this.info = 'Dati salvati con sucesso';
    }

   validateBet (player: Player) {
     let i: number;

     this.betPuntateMap.forEach((puntate: Puntata [], bet: Bet) => {
        for (i = 0; i < puntate.length; i++) {
          if (puntate[i].player.id === player.id &&
            puntate[i].status === 'pending') {

               this.WinOrLose (puntate[i], player);
               this.updateBet (bet);
          }
        }
     });
     }


    updateBet(bet: Bet) {
      this.betService.updateBet(bet).subscribe(() => this.reload());
    }


    WinOrLose(puntata: Puntata, player: Player) {
      if   (puntata.nGol  ===  player.goals ) {
       puntata.status = 'won';
      } else {
        puntata.status = 'lost' ;
     }      
    }


    getPuntateOfBet (index: number) {
      return this.bets[index].getPuntate();
    }


    getBets () {
     this.betService.getBets().subscribe(
       bets => {
         this.bets = bets;
         this.mapPuntate();
    });
    }


    mapPuntate () {
       let i: number;
          for (i = 0; i < this.bets.length; i++) {
              this.betPuntateMap.set (this.bets[i], this.bets[i].puntate);
          }
    }


  }
