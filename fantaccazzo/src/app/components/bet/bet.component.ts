import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Bet} from '../../models/bet';
import { Puntata } from '../../models/puntata';
import {User} from '../../models/user';
import {BetService} from '../../services/impl/bet/bet.service';
import {UserService} from '../../services/impl/user/user.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';



@Component({
  selector: 'app-bet',
  templateUrl: './bet.component.html',
  styleUrls: ['./bet.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BetComponent implements OnInit {


  constructor(private userService: UserService,
    private betService: BetService,
    private router: Router,
    private location: Location) {}

  protected bets: Bet[] = [];
  protected loggedUser: User;
  protected loggedBet: Bet;
  protected info: string;
  protected no_logged_user: string;


  ngOnInit() {
    console.log("bet_init");
    this.getBets();
  }

  deleteTrigger(bet: Bet){
    console.log("trigger");
    this.deleteBet(bet);
  }
  
  updateTrigger(bet: Bet){
    this.loggedBet = bet;
    this.addBet();
  }
  
  goBack(){
    this.location.back();
  }
  getBets(): void {
    console.log("getBets");
        this.loggedUser = this.userService.getLoggedUser();
    console.log(this.loggedUser);
    if(!this.loggedUser){
      console.log("sto in if");
      this.no_logged_user = "Oooops sembra che tu non sei loggato";
      console.log(this.no_logged_user);
    }
    console.log(this.loggedUser);
    this.betService.getBets().subscribe(bets => {console.log("aaaaaaaaaaaa00"); this.bets = bets; console.log(this.bets);});
    // this.userService.getLoggedUser().subscribe(user => this.loggedUser = user);
  }
  
  goToAdd(): void {
    this.loggedBet = new Bet();
    this.loggedBet.setUser(this.loggedUser);
    this.addBet();
    const p = new Puntata();
    this.loggedBet.puntate.push(p);
    this.bets.push(this.loggedBet);
  }

  addBet(): void {
    this.loggedBet.puntate = this.loggedBet.puntate.filter(p => p.player !== undefined && p.nGol !== undefined);
    console.log(JSON.stringify(this.loggedBet.puntate));
    this.betService.addBet(this.loggedBet).subscribe(() => console.log("added bet"));
  }



  gotoLogin(){
    this.router.navigate(['/login']);
  }
  
  deleteBet(bet): void {
    //    this.selectedBet = bet;
    this.bets = this.bets.filter(b => b !== bet);
    this.betService.deleteBet(bet.id).subscribe(() => {console.log("deleted bet");});
  }

  isThereUserLoggedBet(): boolean {
    let i: number;
    for (i = 0; i < this.bets.length; i++) {
      if (this.bets[i].user.id === this.loggedUser.id) {
        return this.bets[i].puntate.length !== 0;
      }
    }
    return false;
  }


}

