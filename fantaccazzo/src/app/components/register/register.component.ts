import { Message } from '../../models/message';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { MessageService } from '../../services/impl/message/message.service';
import { UserService } from '../../services/impl/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  
  myForm: FormGroup;
  message: Message;
  
  constructor(private fb: FormBuilder,
              private messageService: MessageService,
              private router: Router,
              private userService: UserService) { 
  
       this.myForm = fb.group({
         
            'firstname': ['', Validators.required],
            'lastname':  ['', Validators.required],
            'username':  ['', Validators.required],
            'password':  ['', Validators.required]
       });
  }

  ngOnInit() {
  }
  
  register(form: any) {
    
      console.log('RegisterComponent->register(), firstname: ', form.firstname);
      console.log('RegisterComponent->register(), lastname: ', form.lastname);
      console.log('RegisterComponent->register(), username: ', form.username);
      console.log('RegisterComponent->register(), password: ', form.password);
      
      let user = new User();
      let self = this;
    
      user.firstName = form.firstname;
      user.lastName = form.lastname;
      user.userName = form.username;
      user.passWord = form.password;
    
      this.userService.addUser(user)
                      .subscribe(
                        user => {
                          
                          console.log('RegisterComponent->register(), user = ', user);
                            
                            this.message = this.messageService.success('Registrazione avvenuta con successo');                        
                            setTimeout(function() {
                                          self.router.navigate(['/login']);
                            }, 2000);
                         
                        },
                        error => {
                          console.log('RegisterComponent->register(), error = ', error);
                          this.message = this.messageService.error('Errore nella registrazione');
                        });
    
  }

}
