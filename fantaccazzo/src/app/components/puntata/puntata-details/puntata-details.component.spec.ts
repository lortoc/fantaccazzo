import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntataDetailsComponent } from './puntata-details.component';

describe('PuntataDetailsComponent', () => {
  let component: PuntataDetailsComponent;
  let fixture: ComponentFixture<PuntataDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntataDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntataDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
