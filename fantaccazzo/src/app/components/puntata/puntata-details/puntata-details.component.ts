import {Bet} from '../../../models/bet';
import {Player} from '../../../models/player';
import {Puntata} from '../../../models/puntata';
import {User} from '../../../models/user';
import {BetService} from '../../../services/impl/bet/bet.service';
import {PlayerService} from '../../../services/impl/player/player.service';
import {TeamService} from '../../../services/impl/team/team.service';
import {Component, OnInit, ViewEncapsulation, Inject, Input, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-puntata-details',
  templateUrl: './puntata-details.component.html',
  styleUrls: ['./puntata-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PuntataDetailsComponent implements OnInit {

  constructor(private betService: BetService, private teamService: TeamService,
    private playerService: PlayerService, private router: Router) {}

  @Input()
  protected puntata: Puntata;
  @Input()
  protected betSelected: Bet;
  @Input()
  protected loggedUser: User;
  @Input()
  protected user: User;

  protected info: string;
  protected no_logged_team = '';
  protected players: Player[];

  @Output()
  deleteBetEvent: EventEmitter<Bet> = new EventEmitter<Bet>();

  @Output()
  updateBetEvent: EventEmitter<Bet> = new EventEmitter<Bet>();

  ngOnInit() {
    console.log("userssss");
    console.log(this.loggedUser.id);
    console.log(this.user.id);
    this.teamService.getTeamFromUser(this.user.id).subscribe(team => {
      if (team[0]) {
        this.players = team[0].players; console.log(this.players);
      }
      else
        this.no_logged_team = "Sembra che non hai ancora creato una squadra";
    });
  }

  addPuntata(): void {
    this.info = '';
    const p = new Puntata();
    this.betSelected.puntate.push(p);
  }

  deletePuntata(): void {
    this.betSelected.puntate = this.betSelected.puntate.filter(p => p !== this.puntata);
    if (this.betSelected.puntate.length === 0)
      this.deleteBetEvent.emit(this.betSelected);
    else {
      this.betService.updateBet(this.betSelected).subscribe(() => {
        console.log("deleted puntata");
      });
    }
  }

  gotoTeams() {
    this.deleteBetEvent.emit(this.betSelected);
    this.router.navigate(['/teams']);
  }

  updatePuntata() {
    this.updateBetEvent.emit(this.betSelected);
    //    this.betService.updateBet(this.betSelected).subscribe(() => {
    //      console.log("here");
    //
    this.info = 'Scommessa modificata con successo ';
    //      //      for (let puntata of this.betSelected.puntate) {
    //      //        if (puntata.player) {
    //      //          this.info = this.info + ' ' + puntata.player.name + ' - ' + puntata.player.ruolo + ' goals: ' + puntata.nGol;
    //      //        }
    //      //      }
    //    });
  }
  byId(item1: Player, item2: Player) {
    if (item1 && item2)
      return item1.id === item2.id;
  }
}
