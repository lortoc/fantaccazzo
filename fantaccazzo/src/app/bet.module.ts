// import { BetAddComponent } from './components/bet/bet-add/bet-add.component';
// import { BetDetailsComponent } from './components/bet/bet-details/bet-details.component';
import { BetComponent } from './components/bet/bet.component';
import { PuntataDetailsComponent } from './components/puntata/puntata-details/puntata-details.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BetService } from './services/impl/bet/bet.service';
import { PlayerService } from './services/impl/player/player.service';
import { TeamService } from './services/impl/team/team.service';
import { UserService } from './services/impl/user/user.service';

import { InMemoryDataService } from './services/mocks/in-memory-data.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import {HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
    InMemoryDataService, { dataEncapsulation: false },
      ),
    FormsModule
              ],
  declarations: [
      BetComponent,
//      BetDetailsComponent,
//      BetAddComponent,
      PuntataDetailsComponent
  ],
  exports: [BetComponent, 
//      BetDetailsComponent],
    ],
  providers: [
    BetService,
    UserService,
    TeamService,
    PlayerService

  ]
})
export class BetModule { }
