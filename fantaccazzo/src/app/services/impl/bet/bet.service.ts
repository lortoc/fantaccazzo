import { Bet } from '../../../models/bet';
import { IBetService } from '../../interfaces/ibetservice';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class BetService implements IBetService{ 

protected betsUrl = 'api/bets';

  constructor(private http: HttpClient) { }

  getBets(): Observable<Bet[]> {
    console.log("here bet service");
    return this.http.get<Bet[]>(this.betsUrl);
}

  getBetById(id: number): Observable<Bet> {
    const url = this.betsUrl + '/' + id;
    console.log(url);
    return this.http.get<Bet>(url);
  }
  updateBet(bet: Bet): Observable<any> {
    return this.http.put(this.betsUrl, bet, httpOptions);
  }
  
  deleteBet(id: number): Observable<any> {
    return this.http.delete(this.betsUrl+ '/' + id);
  }
  

  addBet(bet: Bet): Observable<Bet> {
    return this.http.post<Bet>(this.betsUrl, bet, httpOptions);
  }
}

