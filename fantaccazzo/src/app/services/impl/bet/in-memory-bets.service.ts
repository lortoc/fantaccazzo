import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const bets = [
    {id: 1,
     user:{id: 1, name: 'Lorenzo'},
     puntate: [
          { player:
          {name: 'Daniele De Rossi' , ruolo: 'cc'},
          nGol : 3 }
              ] },
      {id: 2,
         user:{id: 2, name: 'Nigel'},
         puntate: [
             { player:
             {name: 'Gonzalo Higuain' , ruolo: 'p'},
             nGol : 0 }
              ] },
      {id: 3,
         user:{id: 3, name: 'Emanuele'},
         puntate: [
       { player:
         {name: 'Lorenzo Insigne' , ruolo: 'p'},
          nGol : 1 }
       ] },
      {id: 4, 
        user:{id: 4, name: 'Mauro'},
        puntate: [
       { player:
         {name: 'Edin Dzeko' , ruolo: 'p'},
          nGol : 4 }
       ] }
    ];
    
        const teams = [
      {
      id: 1,
      name: 'AC tua',
      user: {id:1, name: 'Lorenzo'},
      players: [
      {name: 'Daniele De Rossi', ruolo: 'cc'},
      {name: 'Francesco Totti', ruolo: 'p'}
      ]}
    ];
    
    return {bets, };
  }
}
