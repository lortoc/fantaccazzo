import { Player } from '../../../models/player';
import { IPlayerService } from '../../interfaces/iplayerservice';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class PlayerService implements IPlayerService {

  protected playersUrl = 'api/players';

  constructor(private http: HttpClient) { }

  getPlayersFromTeam(id: number): Observable<Player[]> {
    const url = 'api/teams' + '/' + id + '/players';
    return this.http.get<Player[]>(url);
  }

  getPlayerById(id: number): Observable<Player> {
    const url = this.playersUrl + '/' + id;
    return this.http.get<Player>(url);
  }

    getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.playersUrl);
  }

}
