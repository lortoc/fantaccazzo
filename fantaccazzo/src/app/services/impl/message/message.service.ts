import { Message } from '../../../models/message';
import { IMessageService } from '../../interfaces/imessageservice';
import { Injectable } from '@angular/core';

@Injectable()
export class MessageService implements IMessageService {
  
  success(text: string): Message {
    
    let message = new Message();
    message.text = text;
    message.level = 'success';
    return message;
    
  }
  
  error(text: string): Message {
    
    let message = new Message();
    message.text = text;
    message.level = 'error';
    return message;
    
  }

  constructor() { }

}
