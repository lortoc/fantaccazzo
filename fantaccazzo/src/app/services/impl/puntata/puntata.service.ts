import { Puntata } from '../../../models/puntata';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { IPuntataService } from '../../interfaces/ipuntataservice';
import {BetService} from '../bet/bet.service';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PuntataService implements IPuntataService {

protected puntataUrl = 'api/puntate';

  constructor(private http: HttpClient) { }

  getPuntate(): Observable<Puntata[]> {
    return this.http.get<Puntata[]>(this.puntataUrl);
}


 getPuntateByStatus(status: String):  Observable<Puntata[]> {
       const url = this.puntataUrl + '/?status=' + status;
       console.log(url);
return this.http.get<Puntata[]>(url);
 }


  updatePuntata(puntata: Puntata): Observable<any> {
    console.log(puntata);
        console.log(this.puntataUrl);
    return this.http.put(this.puntataUrl, puntata.player, httpOptions);
  }


// Not working
getPuntataByPlayer(id: number): Observable<Puntata> {
       const url = this.puntataUrl + '/?player=' + id;
return this.http.get<Puntata>(url);
 }

}

