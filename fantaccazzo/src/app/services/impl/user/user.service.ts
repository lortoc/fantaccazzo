import { User } from '../../../models/user';
import { IUserService } from '../../interfaces/iuserservice';
import { MessageService } from '../message/message.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService implements IUserService {
  
  usersUrl = 'api/users';  // URL to web api
  users: User [];
  
  constructor(private http: HttpClient,
              private messageService: MessageService) {
  
  }
  
  addUser(user: User) {
    
    let success = false;
    
    let usersObservable = this.http.get<User[]>(this.usersUrl);
      
     usersObservable.subscribe(users => {
      
      this.users = users;
      console.log('addUser, users.length = ', this.users.length);
      
      if(this.users && this.users.length &&  this.users.length > 0){
        let count = this.users.length;
        user.id = ++count;
        
      }
            
    });
    
    console.log('usersObservable = ', usersObservable);
    
       
    return this.http.post(this.usersUrl, user);
                  
  }

  getLoggedUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  
}