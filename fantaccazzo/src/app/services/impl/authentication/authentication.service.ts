import { User } from '../../../models/user';
import { IAuthenticationService } from '../../interfaces/iauthenticationservice';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Injectable()
export class AuthenticationService implements IAuthenticationService {

  usersUrl = 'api/users';  // URL to web api
  
  constructor(private http: HttpClient) {

  }

  login(username: string, password: string) {

    console.log('AuthenticationService->login()...username = ', username);
    console.log('AuthenticationService->login()...password = ', password);
  
    const url = `${this.usersUrl}/?userName=${username}&passWord=${password}`;
    
    console.log('AuthenticationService->login()...url = ', url);

    return this.http.get<User[]>(url);
     
  }
  
  private handleError (error: any) {
      
      console.error(error);
      return Observable.throw(error);
  }

  logout(): void {
    // elimino l'utente corrente dal local storage
    localStorage.removeItem('currentUser');
  }

  

}
