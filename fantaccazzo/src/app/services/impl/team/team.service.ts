import { Team } from '../../../models/team';
import { ITeamService } from '../../interfaces/iteamservice';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class TeamService implements ITeamService{

  constructor(private http: HttpClient) { }

  getTeamById(id:number): Observable<Team>{
    const url = 'api/teams/' + id;
    return this.http.get<Team>(url);
  }
  
  getTeamFromUser(id: number): Observable<Team>{
    const url = 'api/teams?user=' + id;
    console.log(url);
    return this.http.get<Team>(url);
  }
}
