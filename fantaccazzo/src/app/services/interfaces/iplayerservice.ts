import { Player } from '../../models/player';
import { Observable } from 'rxjs/Observable';

export interface IPlayerService {
  getPlayersFromTeam(id:number): Observable<Player[]>;
  getPlayerById(id:number): Observable<Player>;
}
