import { User } from '../../models/user';

export interface IUserService {
  getLoggedUser(): User;
  addUser(user: User);
}
