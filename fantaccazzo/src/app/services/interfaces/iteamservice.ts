import { Team } from '../../models/team';
import { Observable } from 'rxjs/Observable';
export interface ITeamService {
  getTeamFromUser(id:number): Observable<Team>;
  getTeamById(id:number): Observable<Team>;
}
