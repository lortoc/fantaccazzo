import { Message } from '../../models/message';

export interface IMessageService {
  success(text: string):Message;
  error(text: string):Message;
}