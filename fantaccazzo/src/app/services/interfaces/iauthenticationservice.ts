import { User } from '../../models/user';
import { Observable } from 'rxjs/Observable';

export interface IAuthenticationService {
  login(username: string, password: string);
  logout(): void;
}
