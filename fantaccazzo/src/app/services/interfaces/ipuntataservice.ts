import { Puntata } from '../../models/puntata';
import { Observable } from 'rxjs/Observable';

export interface IPuntataService {

  getPuntate(): Observable<Puntata[]>;
  getPuntataByPlayer(id: number): Observable<Puntata>;
  getPuntateByStatus(status: String):  Observable<Puntata[]>;

  updatePuntata(puntata: Puntata): Observable<any>;

}
