import { Bet } from '../../models/bet';
import { Observable } from 'rxjs/Observable';

export interface IBetService { 
  getBets(): Observable<Bet[]>;
  getBetById(id: number): Observable<Bet>;
  updateBet(bet: Bet): Observable<any>;
  addBet(bet: Bet): Observable<Bet>;
  deleteBet(id:number): Observable<any>;
  
}

