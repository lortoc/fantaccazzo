import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryUserService implements InMemoryDbService {
  createDb() {
    const users = [
      {
         id: 1,
         userName: 'mauro',
         passWord: 'mauro',
         firstName: 'Mauro',
         lastName: 'Manno',
         role: 'User'
      },
      {
         id: 2,
         userName: 'lorenzo',
         passWord: 'lorenzo',
         firstName: 'Lorenzo',
         lastName: 'Tocco',
         role: 'User'
      },
      {
         id: 3,
         userName: 'nigel',
         passWord: 'nigel',
         firstName: 'Nigel',
         lastName: 'Plaha',
         role: 'User'
      },
      {
         id: 4,
         userName: 'emanuele',
         passWord: 'emanuele',
         firstName: 'Emanuele',
         lastName: 'Bondatti',
         role: 'User'
      }
    ];
    return {users};
  }
}