import {InMemoryDbService} from 'angular-in-memory-web-api';


export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    
        const users = [
      {
         id: 1,
         userName: 'mauro',
         passWord: 'mauro',
         firstName: 'Mauro',
         lastName: 'Manno',
         role: 'User'
      },
      {
         id: 2,
         userName: 'lorenzo',
         passWord: 'lorenzo',
         firstName: 'Lorenzo',
         lastName: 'Tocco',
         role: 'admin'
      },
      {
         id: 3,
         userName: 'nigel',
         passWord: 'nigel',
         firstName: 'Nigel',
         lastName: 'Plaha',
         role: 'User'
      },
      {
         id: 4,
         userName: 'emanuele',
         passWord: 'emanuele',
         firstName: 'Emanuele',
         lastName: 'Bondatti',
         role: 'User'
      }
    ];
    
    const bets = [

    {id: 11,
     user: {id: 2, userName: 'lorenzo', passWord: 'lorenzo', firstName: 'Lorenzo', lastName: 'Tocco', role: 'Admin'},
     puntate: [
          { id: 1, status: 'pending',
         player:  {id: 1, name: 'Daniele De Rossi' , ruolo: 'cc'},
          nGol : 3,
          }
              ] },
      {id: 2,
         user: { id: 3, userName: 'nigel', passWord: 'nigel', firstName: 'Nigel', lastName: 'Plaha', role: 'User'},
         puntate: [
             {id: 2, status: 'pending',
             player: {id: 6 , name: 'Gonzalo Higuain' , ruolo: 'p'},
             nGol : 0 }
              ] },
      {id: 31,
         user: { id: 4, userName: 'emanuele', passWord: 'emanuele', firstName: 'Emanuele', lastName: 'Bondatti', role: 'User'},
         puntate: [
       { id: 3, status: 'pending', player:  {id: 5, name: 'Lorenzo Insigne' , ruolo: 'p'},
          nGol : 1 }
       ] },

    ];
       const loggedUser = {
      id: 1, name: 'Lorenzo', role: 'admin',
    };
//      const users = [
//      {id: 1, name: 'Lorenzo', team:  {id: 1,
//   name: 'AC tua',
//   user: {id: 1, name: 'Lorenzo'},
//   players: [{name: 'Daniele De Rossi', ruolo: 'cc'}, {name: 'Francesco Totti', ruolo: 'p'}]
//  }
//        },
//      {id: 2, name: 'Nigel'},
//      {id: 3, name: 'Emanuele'},
//      {id: 4, name: 'Mauro'}
//      ];

      const players = [
        {id: 1, name: 'Daniele De Rossi', ruolo: 'cc', goals: 0},
        {id: 2, name: 'Francesco Totti', ruolo: 'p', goals: 0},
        {id: 3, name: 'Edin Dzeko', ruolo: 'p', goals: 0},
        {id: 4, name: 'Aleksander Kolarov', ruolo: 'd', goals: 0},
        {id: 5, name: 'Lorenzo Insigne', ruolo: 'p', goals: 0},
        {id: 6, name: 'Gonzalo Higuain', ruolo: 'p', goals: 0}
      ];

const puntate = [
{ id: 1, status: 'pending', player: {id: 1, name: 'Daniele De Rossi' , ruolo: 'cc'}, nGol : 3 },
{ id: 2, status: 'pending', player: {id: 6 , name: 'Gonzalo Higuain' , ruolo: 'p'},  nGol : 0 },
{ id: 3, status: 'pending', player: {id: 5, name: 'Lorenzo Insigne' , ruolo: 'p'}, nGol : 1 }
];


      const teams = [
  {id: 1,
   name: 'AC tua',
   user: 2,
   players: [{id: 1, name: 'Daniele De Rossi', ruolo: 'cc'}, {id: 2, name: 'Francesco Totti', ruolo: 'p'}]
  },
       {id: 3,
   name: 'Scarsenal',
   user: 3,
   players: [{id: 6, name: 'Gonzalo Higuain', ruolo: 'p'}]
  },
   {id: 2,
     name: 'AS orca',
     user: 4,
     players: [{id : 3, name: 'Edin Dzeko', ruolo: 'p'}, {id: 4, name: 'Aleksander Kolarov', ruolo: 'p'}]
   }
];
    const pagella = [{
     player: {id: 1, name: 'Daniele De Rossi', ruolo: 'cc'},
     goals: 3 
    }];
    
    return {users: users, bets: bets, loggedUser: loggedUser, teams: teams, players: players, puntate: puntate};
  }
}
