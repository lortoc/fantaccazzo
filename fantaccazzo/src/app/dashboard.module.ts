import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './/app-routing.module';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import {AdminAreaComponent} from './components/dashboard/admin-area/admin-area.component';
import {NavbarComponent} from './components/navbar/navbar.component';
@NgModule({
  imports:      [ CommonModule, FormsModule, AppRoutingModule ],
  declarations: [ DashboardComponent, AdminAreaComponent, NavbarComponent ],
  exports:      [ DashboardComponent, AdminAreaComponent, NavbarComponent ],
  providers:    [  ]
})
export class DashboardModule { }

